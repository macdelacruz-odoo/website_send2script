import os
import sys
import subprocess
import re
import time

# Constants
ADDONS_FOLDER = "/opt/odoo/addons/"
ADDON_GIT = "https://gitlab.com/macdelacruz-odoo/website_send2script.git"


# noinspection PyMethodMayBeStatic
class Logger(object):
    def __init__(self):
        pass

    def info(self, message):
        print("INFO: %s" % message)

    def warn(self, message):
        print("WARNING: %s" % message)

    def error(self, message):
        sys.exit("ERROR: %s" % message)


logger = Logger()

# Check if root
if not os.geteuid() == 0:
    logger.error("Must be run as root. Try again: sudo python install_odoo_addon.py")

# Create the addons folder
if not os.path.exists(ADDONS_FOLDER):
    os.makedirs(ADDONS_FOLDER)
    logger.info("Created folder %s" % ADDONS_FOLDER)
else:
    logger.info("Folder %s already created, hence skipping..." % ADDONS_FOLDER)

# Check if git exists
try:
    logger.info("Checking if git is installed...")
    proc1_0 = subprocess.Popen(['git', '--version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    logger.info("Git is found...")
except OSError:
    logger.info("Git not found, attempting to install...")
    # Update the system
    try:
        proc0 = subprocess.Popen(['apt-get', 'update'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        logger.info("Updating the system with apt-get...")
        error = proc0.stdout.read()
        if error:
            logger.warn(error)
    except Exception, e:
        logger.warn(e)

    # Install latest git
    proc1_1 = subprocess.Popen(['apt-get', 'install', '-y', 'git'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    logger.info("Installing git...")
    error = proc1_1.stdout.read()
    if error:
        logger.warn(error)

# Obtain the module
addon_name = ADDON_GIT.rsplit('/', 1)[-1].rsplit('.git', 1)[0]
addon_path = os.path.join(ADDONS_FOLDER, addon_name)
if not os.path.exists(addon_path):
    proc2_0 = subprocess.Popen(
            ['git', 'clone', ADDON_GIT, addon_name],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            cwd=ADDONS_FOLDER)
    logger.info("Cloned repository %s to %s" % (ADDON_GIT, ADDONS_FOLDER))
    error = proc2_0.stdout.read()
    if error:
        logger.warn(error)
else:
    proc2_1 = subprocess.Popen(
        ['git', 'pull', 'origin', 'master'],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        cwd=addon_path)
    error = proc2_1.stderr.read()
    if error:
        logger.warn(error)

# Update the current Odoo configuration
# Find the configuration file
proc3 = subprocess.Popen(['ps', '-ef'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
config_path = re.search(r'\/\S*openerp-server\.conf(?=\s)', proc3.communicate()[0]).group(0)
logger.info("Found Odoo configuration file %s" % config_path)

# Add the addon_path in config
config_file = open(config_path, 'rb')
config_lines = config_file.readlines()
for x in range(0, len(config_lines)):
    config_lines[x] = config_lines[x].replace('\n', '')
    if config_lines[x].startswith('addons_path = '):
        ADDONS_FOLDER = ADDONS_FOLDER.rstrip('/')
        if ADDONS_FOLDER not in config_lines[x]:
            config_lines[x] = config_lines[x].rstrip(',')
            config_lines[x] += ',%s' % ADDONS_FOLDER
        else:
            logger.info("Addon path %s already in config..." % ADDONS_FOLDER)
config_file.close()

# Write to config
config_file = open(config_path, 'wb')
config_file.write('\n'.join(config_lines))
logger.info("Added %s to %s" % (ADDONS_FOLDER, config_path))
config_file.close()

logger.info("Odoo module '%s' is successfully installed..." % addon_name)
# Finally, reboot
confirm = raw_input("Reboot now? Type 'yes' to continue:")
if confirm == 'yes':
    logger.info("Rebooting in 5s...")
    time.sleep(5)
    subprocess.Popen(['reboot'])
# Cancel reboot
else:
    logger.info("Everything is fine!")
    sys.exit()
