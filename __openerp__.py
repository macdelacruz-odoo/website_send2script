# -*- coding: utf-8 -*-
# noinspection PyStatementEffect
{
    'name': "Send2Script Snippet",
    'version': '1.0',
    'summary': 'Run server scripts using parameters inputted by your website visitors',
    'description': """

This module allows you to place on your website custom webform snippet that takes in text
inputted by visitors and feeds it to a custom bash, python or any command-line executable
program on the server.

""",
    'category': 'website',
    'author': 'InsiderMark',
    'website': 'https://www.insidermark.com',
    'depends': ['website'],
    'data': [
        'xml/template_snippets.xml',
        'xml/website_templates.xml',
    ]
}
