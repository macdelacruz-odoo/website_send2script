//noinspection SpellCheckingInspection
odoo.define('website_send2script.send2script', function (require) {

    "use strict";

    var ajax = require('web.ajax');

    $(document).on('click', '.s_send2script_form button', function (e) {
        e.preventDefault();
        var $param_input = $('.s_send2script_form').find('input[name="param"]');
        var block_modal = UIkit.modal.blockUI("Please wait...");
        ajax.jsonRpc(
            '/website_send2script/run_script', "call",
            {"param": $param_input.val()}).then(function (result) {
            $(".uk-modal").hide();
            if (result.status == 'success') {
                $param_input.val('');
            }
            UIkit.modal.alert(result.message.replace(/(?:\r\n|\r|\n)/g, '<br />'));
        });
    })
});