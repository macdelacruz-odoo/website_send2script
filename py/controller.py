# -*- coding: utf-8 -*-
from openerp import http
import os
import subprocess
import tempfile


class Send2Script(http.Controller):
    @http.route(
            '/website_send2script/run_script',
            auth='public', type='json', website=True, methods=['post', 'get'])
    def run_script(self, param):
        """
        Executes any script with visitor-inputted text as parameter

        :param param: str input to the script
        :return: dictionary of
        :rtype:`dict`
        """
        # Prepare
        status = 'danger'
        result, error = False, False
        param = param.strip()
        temp_send2script_path = os.path.join(tempfile.gettempdir(), 'odoo-send2script.txt')

        # Proceed
        if param:
            script_path = "/opt/automata/create_odoo9_vm"
            try:
                process = subprocess.Popen(
                        [script_path, '-c', param],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                result, error = process.communicate()
                if not error:
                    message = result
                    status = 'success'
                else:
                    message = "%s\n\n%s" % (result, error)
                    if "ERROR: " not in error:
                        status = 'success'

            except Exception, e:
                message = e.message or str(e)

            # Create temp file as log
            with open(temp_send2script_path, 'w') as temp_send2script:
                temp_send2script.write("""status: %s
result: %s
error: %s
message: %s""" % (status, result, error, message))
        else:
            message = "Please provide an input!"

        # Conclude
        return {
            'message': message,
            'status': status}
